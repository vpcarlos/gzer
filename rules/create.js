const gitlab = require('../lib/gitlab');
const repo = require('../lib/repo')
const chalk = require('chalk')
const files = require('../lib/files')

module.exports = {
    build: async (commands) => {
        try {
            //Check if current directory is a repository      
            if (files.directoryExists('.git')) {
                console.log(chalk.red('Already a git repository!'));
                process.exit();
            }
            //Creating a gilab project
            let attributes = {
                name: commands.name,
                description: commands.description,
                jobs_enabled: false
            }
            if (commands.group) {
                attributes.namespace_id = await gitlab.get_namespace_id(commands.group)
            }
            const project = await gitlab.create_project(attributes);

            //First load if project-only is not enabled
            if (project && !commands.project) {
                if (commands.branch == "master") {
                    await repo.firstLoadMaster(project.data.ssh_url_to_repo, commands.message);
                } else {
                    await repo.firstLoad(project.data.ssh_url_to_repo, commands.branch, commands.message);
                }
            }
        } catch (e) {
            console.log(e);
        }
    }
}