const gitlab = require('../lib/gitlab');
module.exports = {
    build: async (commands) => {
        try {
            attrs = {
                per_page: 100
            }

            let attr_names = []
            if (commands.project) {
                attrs.search = commands.project
            }
            if (commands.ssh) {
                attr_names.push('ssh_url_to_repo')
            }
            const group = await gitlab.get_group(commands.group)
            const projects = await gitlab.get_group_project(group.id, attrs)

            console.table(projects, attr_names)

        } catch (e) {
            console.log(e);
        }
    }
}