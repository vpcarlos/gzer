const chalk = require('chalk')

module.exports = {
    build: (commands, conf) => {
        let strout = ''
        if (commands.show) {
            strout += '\n\n----------CREDENTIALS----------\n\n' +
                'ssh-key-path → ' + (conf.get('gitlab.sshKey') + '\n' || 'Not set yet\n') +
                'gitlab-token → ' + (conf.get('gitlab.token') + '\n' || 'Not set yet\n')
            console.log(chalk.cyan(strout))
        }
    }
}