# Gzer

Manage gitlab projects on one comand line

## Installation

```
# Install Node.js
# npm install -g → Install project and enable gzer comand globally

```
## Comands
### create
```
    Options:
        -g, --group [group]              Group name (default: false)
        -b, --branch [branch]            Branch (default: "master")
        -n, --name [name]                Project name (default: current_directory_name)
        -d, --description [description]  Project description (default: "")
        -m, --message [message]          Inital commit message (default: "Inital load")
        -p, --project                    Create a repository in gitlab (default: false)
        -h, --help                       output usage information 
    Examples:
        $ gzer create -g demo_group -b test -d "Gzer demo project" -m "First load"
        $ gzer create     
```

### credentials
```
    Options:
        -s, --show  Show all crendentials
        -h, --help  output usage information
    Examples:
        $ gzer credentials -s 
```   

