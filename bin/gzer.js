const files = require('../lib/files')
const auth = require('../lib/auth')
const gitlab = require('../lib/gitlab');
const create = require('../rules/create')
const update = require('../rules/update')
const list = require('../rules/list')
const credentials = require('../rules/credentials')

module.exports = class Gzer {

    constructor(program, config) {
        this.program = program
        this.config = config
    }

    async auth(conf) {
        try {
            //Getting gitlab token and ssh key
            const user = await auth.getAuth(conf);
            //Setting ssh key
            await auth.addSshIdentity(user.sshKey)
            //Settign axios default options (API url and token)
            gitlab.set_defaults('https://gitlab.com/api/v4', user.token);
        } catch (e) {
            console.log(e)
            process.exit()
        }
    }

    async commandCreate() {
        this.program.command('create')
            .description('Create a project, initialze local repository and push to remote.')
            .option('-g, --group [group]', 'Group name', false)
            .option('-b, --branch [branch]', 'Branch', 'master')
            .option('-n, --name [name]', 'Project name', files.getCurrentDirectory())
            .option('-d, --description [description]', 'Project description', '')
            .option('-m, --message [message]', 'Inital commit message', 'Inital load')
            .option('-p --project', 'Create project only', false)
            .action((commands) => {
                create.build(commands)
            });
    }

    async commandList(){
        this.program.command('list')
            .description('List')
            .option('-g, --group [group]', 'Group name', false)
            .option('-p,--project [project]','Project name',false)
            .option('-s, --ssh','SSH clone path' ,false)
            .action((commands) => {
                list.build(commands)
            });
    }

    async commandCredentials() {
        this.program.command('credentials')
            .description('Manage gzer credentials')
            .option('-s, --show', 'Show all crendentials', false)
            .action((commands) => {
                credentials.build(commands, this.config)
            });
    }
    async commandUpdate(){
        this.program.command('update')
            .description('Update submodules')
            .option('-g, --group [group]', 'Group name', false)
            .option('-b, --branch [branch]', 'Branch', 'master')
            .option('-p, --project [project]', 'Project name', 'environments')
            .option('-s --submodule', 'Submodule name', false)
            .action((commands) => {
                update.build(commands)
            });
    }

    async init() {
        await this.auth(this.config)
        //Commands
        this.commandCreate()
        this.commandList()
        this.commandCredentials()
        this.program.parse(process.argv)
    }
}