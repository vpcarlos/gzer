#!/usr/bin/env node

const program = require('commander')
const Configstore = require('configstore');
const pkg = require('../package.json');
const config = new Configstore(pkg.name)
const Gzer = require('./gzer')

const gzer = new Gzer(program, config)
gzer.init()