const inquirer = require('inquirer');

module.exports = {
    askGitlabToken: () => {
        const questions = [{
                name: 'gitlabToken',
                type: 'input',
                message: 'Enter your Gitlab token:',
                validate: function (value) {
                    if (value.length) {
                        return true;
                    } else {
                        return 'Please enter your Gitlab Token';
                    }
                }
            }
        ];
        return inquirer.prompt(questions);
    },
    askSshKeyPath: () => {
        const questions = [{
                name: 'sshKey',
                type: 'input',
                message: 'Enter your ssh key path:',
                validate: function (value) {
                    if (value.length) {
                        return true;
                    } else {
                        return 'Please enter your ssh key path';
                    }
                }
            }
        ];
        return inquirer.prompt(questions);
    }
}