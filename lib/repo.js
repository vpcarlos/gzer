const git = require('simple-git');
const chalk = require('chalk');
const CLI = require('clui');
const Spinner = CLI.Spinner;

module.exports = {
    firstLoad: async (url, branch, initialCommit) => {
            const status = new Spinner(chalk.yellow('Initializing local repository and pushing to remote...'));
            status.start();
            try {
                git()
                    .init()
                    .addRemote('origin', url)
                    .add('./*')
                    .commit(initialCommit)
                    .branch([branch])
                    .checkout(branch)
                    .push(['-u', 'origin', branch])
                    .exec(() => {
                        status.stop();
                        console.log(chalk.green(' ✔ Project pushed'))
                        return true
                    })
            } catch (e) {
                console.log(e.data);
            }
        },

        firstLoadMaster: async (url, initialCommit) => {
            const status = new Spinner(chalk.yellow('Initializing local repository and pushing to remote...'));
            status.start();
            try {
                git()
                    .init()
                    .addRemote('origin', url)
                    .add('./*')
                    .commit(initialCommit)
                    .push(['-u', 'origin', 'master'])
                    .exec(() => {
                        status.stop();
                        console.log(chalk.green(' ✔ Project pushed'))
                        return true
                    })
            } catch (e) {
                console.log(e.data);
            }
        },
}