
const shell = require('shelljs')
const inquirer = require('./inquirer');
setSshKey = async (conf) => {
    const sshKey = await inquirer.askSshKeyPath();
    conf.set('gitlab.sshKey', sshKey.sshKey);
}
setGitlabToken = async (conf) => {
    const token = await inquirer.askGitlabToken();
    conf.set('gitlab.token', token.gitlabToken);
}
getStoredSshKey = async (conf) => {
    if (!conf.get('gitlab.sshKey')) {
        await setSshKey(conf)
    }
    return conf.get('gitlab.sshKey');
}
getStoredGitlabToken = async (conf) => {
    if (!conf.get('gitlab.token')) {
        await setGitlabToken(conf)
    }
    return conf.get('gitlab.token')
}

module.exports = {
    getAuth: async (conf) => {
        return {
            'token': await getStoredGitlabToken(conf),
            'sshKey': await getStoredSshKey(conf)
        }
    },
    addSshIdentity: async (ssh_key_path) => {
        if (shell.exec('ssh-add -l', {
                silent: true
            }).code != 0) {
            return await shell.exec('ssh-add -k ' + ssh_key_path)
        }
    },
}