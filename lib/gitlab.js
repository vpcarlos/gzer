const axios = require("axios");
const CLI = require('clui');
const chalk = require('chalk')
const Spinner = CLI.Spinner;
module.exports = {
    //Gitlab API defaults 
    set_defaults: (baseUrl, token) => {
        axios.defaults.baseURL = baseUrl;
        axios.defaults.headers.common['Private-Token'] = token;
    },

    get_group_project: async (group_id, params = {}) => {
        const status = new Spinner(chalk.yellow('Searching project...'));
        status.start();
        try {
            const project = await axios.get('/groups/' + group_id + '/projects', {
                params
            });
            status.stop();
            if (project.data.length == 0) {
                console.log(chalk.red('Project ' + params.search + ' not found'));
                process.exit();
            }
            console.log(chalk.green(' ✔ ' + project.data.length + ' projects found'));
            return project.data
        } catch (error) {
            status.stop();
            console.error(error);
            process.exit();
        }
    },

    get_group: async (group_name) => {
        const status = new Spinner(chalk.yellow('Searching group...'));
        status.start();
        try {
            const group = await axios.get('/groups', {
                params: {
                    search: group_name
                }
            });
            status.stop();
            if (group.data.length == 0) {
                console.log(chalk.red('Group ' + group_name + ' not found'));
                process.exit();
            }
            return group.data[0]
        } catch (error) {
            status.stop()
            console.error(chalk.red(error))
            process.exit()
        }
    },

    //Gitlab namespace
    get_namespace_id: async (namespace_name) => {
        const status = new Spinner(chalk.yellow('Searching group...'));
        status.start();
        try {
            const namespace = await axios.get('/namespaces', {
                params: {
                    search: namespace_name
                }
            });
            status.stop();
            return namespace.data[0].id
        } catch (error) {
            status.stop();
            console.error(chalk.red(error))
            process.exit();
        }
    },

    //Create Project
    create_project: async (attributes) => {
        const status = new Spinner(chalk.yellow('Creating project...'));
        status.start();
        try {
            const project = await axios.post('/projects', attributes);
            status.stop()
            console.log(chalk.green(' ✔ Project ' + attributes.name + ' created '));
            return project
        } catch (error) {
            status.stop()
            console.error(chalk.red('\n Project ' + error.response.data.message.name));
            process.exit();
        }
    }
}